---
title: "How to Contribute"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

The first step to get involved in the OSPO Alliance is joining our **mailing list**. The archive is public and subscription is free and open to all.

* OSPO Zone public mailing list: https://accounts.eclipse.org/mailing-list/ospo.zone
* OSPO Zone mailing list archive: http://www.eclipse.org/lists/ospo.zone

You can also collaborate with the OSPO Alliance on several assets:

- The **OSPO.Zone website**, which is managed as [an open source Eclipse project](https://www.eclipse.org/projects/handbook/). The Eclipse Plato project is hosted on the Eclipse GitLab forge, and questions, issues and contributions are welcome. See https://gitlab.eclipse.org/eclipse/plato.

- The **OW2 Good Governance Initiative** holds the collaborative work on the GGI Body of Knowledge. It is [hosted at OW2](https://www.ow2.org/view/OSS_Governance/) and has [its own mailing list](https://mail.ow2.org/wws/info/ossgovernance) for discussions related to the development of the method. It is, of course, free and open to all.

- **Feedback is important** to us, so please feel free to comment on the GGI [Body of Knowledge activities](/ggi/activities/) for specific questions or remarks, or on the mailing lists for more general-purpose discussions. 

- Got a mature OSPO already? We need case studies about best practices and, perhaps unexpectedly, not-so-good practices (e.g. things that didn't work out). 

Please note that we have, and enforce, a [code of conduct](https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php) to ensure a respectful, inclusive and welcoming community. Some parts of the site, like [OnRamp](/onramp/), also use the [Chatham House rule](https://www.chathamhouse.org/about-us/chatham-house-rule), so please be responsible when quoting people here.

