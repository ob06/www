---
title: "OSPO Alliance Celebrates New Supporters Including Red Hat"
date: 2021-07-27T08:00:00+02:00
show_featured_footer: false
layout: "single"
---

**THE INTERNET, July 27, 2021**

The OSPO Alliance today announced that major international open source contributors, including Red Hat, Microsoft and Orange, have signed up to participate in the Alliance's activities and in OSPO.Zone. Twelve organisations have signed statements of support for OSPO Alliance since it launched in June. The strength of support confirms the importance of open source software to the European economy and the value of establishing a centre of excellence promoting good open source governance in every organisation.

The OSPO Alliance aims to help companies and public institutions discover and understand open source, start benefiting from it across their activities and grow to host an Open Source Program Office (OSPO).  The OSPO Alliance provides an organisation-neutral venue for contributors to share resources, experience and skills. Supporters can opt to contribute at the new OSPO.Zone website and/or contribute to the articulation of the [Good Governance Initiative](https://www.ow2.org/view/OSS_Governance/) methodology.

### Quotations from new supporters

#### Red Hat

> “As Red Hat’s own twenty-eight year history is built upon open collaboration, governance, and code, we’re  pleased to join the OSPO Alliance and help foster education for open source participation across > industries. The Alliance embodies the spirit and the practice of open collaboration in a vendor-neutral manner, bringing together open source leaders to help guide organizations looking to embark upon or refresh their open source strategy. Red Hat looks forward to contributing to the OPSO Alliance’s shared knowledge base and strengthening collaboration in good governance practices.” said Deb Bryant, Sr Director, Open Source Program Office, Red Hat

#### Microsoft 

> “Open source is a core part of nearly every company’s digital strategy today. Across all of our teams at Microsoft, we collaborate with open source communities to use, contribute to, and release open source software to help organizations achieve more. We’re committed to learning and growing together with the open source ecosystem so we can continue to make open source sustainable for everyone. We have supported and contributed to the OW2 Good Governance initiative from its inception and look forward to continuing to work with the OSPO Alliance to further advance these practices in Europe.” said Stormy Peters, Director, Open Source Programs Office at Microsoft

#### Orange 

> "As a founding member of OW2, Orange has long recognised the importance of open source software. After helping kick start the OW2 Good Governance Initiative, the Orange Group looks forward to working with the OSPO Alliance to help others improve their professional and responsible management of open source." said Christophe Jelen, head of Open Source Governance for Orange and Julien Dubreuil, head of the OSPO at Orange Business Services.

#### Engineering Group 	
> "As a global digital services player for more than 40 years we are, with our Knowage division among others, a determined member and contributor to the open source ecosystem. With a strong focus on open source management, we recognise that we can do much more to facilitate adoption by our customers. For this reason we regard the OSPO Alliance as an exciting new European initiative for us to get involved in, strengthening our historic collaboration with OW2, Eclipse and the other open source major actors." said Orazio Viele, CTO & Research and Innovation Director at Engineering Group.

#### OpenUK
> “OpenUK recognises the importance of Open Source Program Offices in ensuring good governance and managing security of sustainable open source software and so is collaborating with the OSPO Alliance, to support the UK’s evolution of OSPO’s. OpenUK's 2021 Report established the UK as the number one contributor to open source software in Europe with up to £43.1bn being contributed to annual GDP in the UK economy. There has never been a more important time for the UK to support the evolution of its open source contribution and usage than today in a post Brexit Global Britain, focusing on good local practices to allow collaboration with our open source colleagues globally.”  said Amanda Brock, CEO, OpenUK

#### Bitergia
> “Bitergia is excited to join the OSPO Alliance as an open initiative that aims at enhancing the adoption of sound open source development methods. With more than 15 years of experience in the different open source communities, Bitergia values this openness and diversity of knowledge sources as key principles that have always been part of open source DNA and the foundations for open source success as a way of working.” said Daniel Izquierdo Cortázar, CEO of Bitergia

### Quotations from Founders

#### Open Forum Europe
> “OpenForum Europe is proud to be a founding supporter of the OSPO Alliance to help strengthen the institutional capacity of global open source. Well-run Open Source Programme Offices hold massive potential to enable open innovation in society at large. This goes for private companies, be it small or large software vendors or digitising industrial companies, as well as the public sector and academia. We see the OSPO Alliance as an important forum to realise the potential of the OSPO construct as a fundamental building block and networking interface in the global institutional infrastructure of open source across sectors.” said Sachiko Muto, CEO, Open Forum Europe

#### Foundation for Public Code
> We’re proud to be a co-founder of the OSPO Alliance, a valuable resource for knowledge sharing and open collaboration. Public sector digital transformation requires strong operational mechanisms to connect public administrations to the open source ecosystem. The ability for these organizations to learn from each other, industry and academia is essential for adopting open source in the public sector. We’re confident that the OSPO Alliance will make implementing open source in the public sector more practical and achievable.” said Boris van Hoytema, Chief Executive, Foundation for Public Code

### About the OSPO Alliance

The OSPO Alliance was established by leading European open source non-profit organisations, including OW2, Eclipse Foundation, OpenForum Europe, and Foundation for Public Code, and experienced practitioners with the aim to grow awareness for open source in Europe and to globally promote structured, responsible and professional management of open source by companies and administrations. OSPO.Zone is the new website for delivering the resources and collaboration envisaged by the OSPO Alliance. Learn more at https://ospo.zone.

### Media contacts

Schwartz Public Relations  
Sendlinger Straße 42A  
80331 Munich  
EclipseFoundation@schwartzpr.de  
Julia Rauch / Sophie Dechansreiter / Tobias Weiß  
+49 (89) 211 871 – 43 / -35 / -70
